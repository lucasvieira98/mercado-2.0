import http from "@/plugins/axios";

export default {
  index() {
    return http.get('/postssss')
  },

  show(id){
    return http.get(`/posts/${id}`)
  },

  create(new_bacon){
    return http.post('/posts',{
      bacon: {
        ...new_bacon
      }
    })
  },

  update(bacon){
    return http.put(`/posts/${bacon.id}`,{
      bacon: {
        ...bacon
      }
    })
  },

  delete(id){
    return http.delete(`/posts/${id}`)
  },



}
