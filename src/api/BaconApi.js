import http from "@/plugins/axios";

export default {
  index() {
    return http.get('/posts')
  },

  show(id){
    return http.get(`/posts/${id}`)
  },

  create(new_post){
    return http.post('/posts',{
      post: {
        ...new_post
      }
    })
  },

  update(post){
    return http.put(`/posts/${post.id}`,{
      post: {
        ...post
      }
    })
  },

  delete(id){
    return http.delete(`/posts/${id}`)
  },



}
