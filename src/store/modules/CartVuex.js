const Cart = {
  namespaced: true,
  state: {
    allCarts: [], // [],

    oneCart: {
      id: null,
      owner: '',
      n_items: 0,
      items: [],
      value: 0,
    }, // {}

    success: null,
    success_msg: null,
    error: null,
    error_msg: null,
  },

  getters: {
    getAllCarts: state => {
      return state.allCarts
    },

    getUnderReviewCarts: state => {
      return state.allCarts.filter(c => c.status == 'under_review')
    },

    getApprovedCarts: state => {
      return state.allCarts.filter(c => c.status == 'approved')
    },

    getRepprovedCarts: state => {
      return state.allCarts.filter(c => c.status == 'disapproved')
    },

    getOneCart: state => {
      return state.oneCart
    },

    getOneCartNumberOfItems: state => {
      return state.oneCart.n_items
    },

    getOneCartItems: state => {
      return state.oneCart.items
    },

    getOneCartValue: state => {
      return state.oneCart.value
    }
  },

  mutations: {
    setOneCart (state, cart) {
      state.oneCart = cart
    },

    setAllCarts (state, allCarts) {
      state.allCarts = allCarts
      console.log(state.allCarts)
    }
  },

  actions: {
    addProduct (context, {product}) {
      const isAlreadyInCart = context.state.oneCart.items.map(item => item.id).indexOf(product.id)

      if (isAlreadyInCart != -1) {
        context.state.oneCart.items[isAlreadyInCart].qtd++
        context.state.oneCart.value += product.value
        alert('Produto já existente no carrinho, adicionado 1 à sua quantidade.')
      } else {
        product.qtd = 1
        context.state.oneCart.items.push({id: product.id, qtd: product.qtd})
        context.state.oneCart.value += product.value
        context.state.oneCart.n_items++
        alert('Produto adicionado ao carrinho.')
      }

      localStorage.setItem('one_cart', JSON.stringify(context.state.oneCart))
    },

    removeProductFromCart (context, {product}) {
      let idx = context.state.oneCart.items.indexOf(context.state.oneCart.items.find(i => i.id == product.id))

      context.state.oneCart.items.splice(idx, 1)
      context.state.oneCart.n_items--
      if (context.state.oneCart.n_items == 0) {
        context.state.oneCart.value = 0
      } else {
        context.state.oneCart.value -= (product.value * product.qtd)
      }

      localStorage.setItem('one_cart', JSON.stringify(context.state.oneCart))

      console.log('Produto removido com sucesso.')
    },

    editItemQtd (context, {product}) {
      let editIdx = context.state.oneCart.items.map(i => i.id).indexOf(product.id)
      let howManyItems = (product.qtd - context.state.oneCart.items[editIdx].qtd)
      context.state.oneCart.items[editIdx].qtd = product.qtd
      context.state.oneCart.value += (product.value * howManyItems)

      localStorage.setItem('one_cart', JSON.stringify(context.state.oneCart))
    },

    submitNewCart (context, {owner, firebase}) {
      context.state.oneCart.owner = owner
      context.state.oneCart.status = 'Em análise'

      localStorage.setItem('one_cart', JSON.stringify(context.state.oneCart))

      firebase.firestore().collection("allCarts").add({
        ...context.state.oneCart
      }).then(() => {
        console.log('Carrinho Finalizado com sucesso')

        Object.assign(context.state.oneCart, {
          id: null,
          owner: '',
          n_items: 0,
          items: [],
          value: 0,
        })

        localStorage.setItem('one_cart', JSON.stringify(context.state.oneCart))
      }).catch(error => {
        console.log(error)
      })
    },

    approveOneItem (context, {product, cart, firebase}) {
      let which_cart = context.state.allCarts.indexOf(context.state.allCarts.find(c => c.id == cart.id))
      let productIdx = context.state.allCarts[which_cart].items.indexOf(context.state.allCarts[which_cart].items.find(p => p.id == product))
      context.state.allCarts[which_cart].items[productIdx].status = 'approved'

      const cartRef = firebase.firestore().collection("allCarts").doc(cart.id)


      let countApproved = 0
      let countDisapproved = 0

      for (let item of context.state.allCarts[which_cart].items) {
        if (item.status == 'approved') {
          countApproved++
        } else if (item.status == 'disapproved') {
          countDisapproved++
        }
      }

      if ((countApproved + countDisapproved) == context.state.allCarts[which_cart].items.length) {
        if (countApproved == context.state.allCarts[which_cart].items.length) {
          context.state.allCarts[which_cart].status = 'approved'
        } else {
          context.state.allCarts[which_cart].status = 'disapproved'
        }
      }

      firebase.firestore().runTransaction(transaction => {
        return transaction.get(cartRef).then(() => {
          transaction.update(cartRef, context.state.allCarts[which_cart])
          return context.state.allCarts[which_cart]
        })
      })

    },

    disapproveOneItem (context, {product, cart, firebase}) {
      let which_cart = context.state.allCarts.indexOf(context.state.allCarts.find(c => c.id == cart.id))
      let idx = context.state.allCarts[which_cart].items.indexOf(context.state.allCarts[which_cart].items.find(p => p.id == product))
      context.state.allCarts[which_cart].items[idx].status = 'disapproved'

      const cartRef = firebase.firestore().collection("allCarts").doc(cart.id)

      let countApproved = 0
      let countDisapproved = 0

      for (let item of context.state.allCarts[which_cart].items) {
        if (item.status == 'approved') {
          countApproved++
        } else if (item.status == 'disapproved') {
          countDisapproved++
        }
      }

      if ((countApproved + countDisapproved) == context.state.allCarts[which_cart].items.length) {
        if (countDisapproved > 0) {
          context.state.allCarts[which_cart].status = 'disapproved'
        }
      }

      firebase.firestore().runTransaction(transaction => {
        return transaction.get(cartRef).then(() => {
          transaction.update(cartRef, context.state.allCarts[which_cart])
          return context.state.allCarts[which_cart]
        })
      })
    },

    approveAllCart (context, {cart, firebase}) {
      let which_cart = context.state.allCarts.indexOf(context.state.allCarts.find(c => c.id == cart.id))

      context.state.allCarts[which_cart].status = 'approved'

      for (let item of context.state.allCarts[which_cart].items) {
        item.status = 'approved'
      }

      const cartRef = firebase.firestore().collection("allCarts").doc(cart.id)

      firebase.firestore().runTransaction(transaction => {
        return transaction.get(cartRef).then(() => {
          transaction.update(cartRef, context.state.allCarts[which_cart])
          return context.state.allCarts[which_cart]
        })
      })
    },

    disapproveAllCart (context, {cart, firebase}) {
      let which_cart = context.state.allCarts.indexOf(context.state.allCarts.find(c => c.id == cart.id))

      context.state.allCarts[which_cart].status = 'disapproved'

      for (let item of context.state.allCarts[which_cart].items) {
        item.status = 'disapproved'
      }

      const cartRef = firebase.firestore().collection("allCarts").doc(cart.id)

      firebase.firestore().runTransaction(transaction => {
        return transaction.get(cartRef).then(() => {
          transaction.update(cartRef, context.state.allCarts[which_cart])
          return context.state.allCarts[which_cart]
        })
      })
    },

    deleteDisapproved (context, {product, cart, firebase}) {
      let which_cart = context.state.allCarts.indexOf(context.state.allCarts.find(c => c.id == cart.id))
      let which_product = context.state.allCarts[which_cart].items.indexOf(context.state.allCarts[which_cart].items.find(p => p.id == product.id))

      context.state.allCarts[which_cart].items.splice(which_product, 1)

      let countApproved = 0
      let countDisapproved = 0

      for (let item of context.state.allCarts[which_cart].items) {
        if (item.status == 'approved') {
          countApproved++
        } else if (item.status == 'disapproved') {
          countDisapproved++
        }
      }

      if ((countApproved + countDisapproved) == context.state.allCarts[which_cart].items.length && (countApproved + countDisapproved) != 0) {
        if (countDisapproved == 0) {
          context.state.allCarts[which_cart].status = 'approved'
        }
      }

      const cartRef = firebase.firestore().collection("allCarts").doc(cart.id)

      firebase.firestore().runTransaction(transaction => {
        return transaction.get(cartRef).then(() => {
          transaction.update(cartRef, context.state.allCarts[which_cart])
          return context.state.allCarts[which_cart]
        })
      })
    },

    index(context, firebase){
      firebase
      let one_cart_local_storage = JSON.parse(localStorage.getItem('one_cart'))

      if (one_cart_local_storage == null) {
        one_cart_local_storage = {
          id: null,
          owner: '',
          n_items: 0,
          items: [],
          value: 0,
        }
      }

      context.commit('setOneCart', one_cart_local_storage)

      let all_carts_firestore = []

      firebase.firestore().collection("allCarts").onSnapshot(res => {
        const changes = res.docChanges()

        changes.forEach(change => {
          if (change.type === 'added') {
            all_carts_firestore.push({...change.doc.data(), id: change.doc.id})
          } else if (change.type === 'removed') {
            let idx = all_carts_firestore.map(item => item.id).indexOf(change.doc.id)

            if (idx != -1) {
              all_carts_firestore.splice(idx, 1)
            }
          } else if (change.type === 'modified') {
            Object.assign(all_carts_firestore[all_carts_firestore.map(o=>o.id).indexOf(change.doc.id)], {...change.doc.data(), id: change.doc.id})
          }
        })
      })

      console.log(all_carts_firestore)
      context.commit('setAllCarts', all_carts_firestore)

    },
    show(){},
    create(){},
    update(){},
    destroy(){},
  }

};

export default Cart;
