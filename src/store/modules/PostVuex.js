import Api from '../../api/index'

const Post = {
  namespaced: true,
  state: {
    value: 'my value',

    onePost: null, //to store SHOW response
    allPosts: null, //to store SHOW response
  },
  getters: {
    getStateValue: state => {
      return state.value;
    }
  },

  mutations: {
    setOnePost(state, Post){
      state.onePost = Post // Post.data
    },
    setAllPosts(state, Posts){
      state.allPosts = Posts // Posts.data
    },

    clearAllPosts(state){
      state.allPosts = null
    },
    clearOnePost(state){
      state.onePost = null
    },
  },

  actions: {
    clearAllPosts({commit}){
      commit('clearAllPosts')
    },
    clearOnePost({commit}){
      commit('clearOnePost')
    },

    index(context, filter) {
      Api.Post.index(filter)
        .then(response => response.data)
        .then(Posts => {
          context.commit('setAllPosts', Posts);
          // context.commit('indexResponse', {msg: '', was_good: true});
        }).catch(function(error) {
          // context.commit('indexResponse', {msg: error.response.data.errors[0], was_good: false});
          console.log(error);
        });
    },
    show(context, Post_id) {
      Api.Post.show(Post_id)
        .then(response => response.data)
        .then(Post => {
          context.commit('setOnePost', Post);
          // context.commit('showResponse', {msg: '', was_good: true});
        }).catch(function(error) {
          // context.commit('showResponse', {msg: error.response.data.errors[0], was_good: false});
          console.log(error);
        });
    },
    create(context, new_Post) {
      Api.Post.create(new_Post)
        .then(response => response.data)
        .then(Post => {
          context.commit('setOnePosts', Post); // not often used
          // context.commit('createResponse', {msg: '', was_good: true});
        }).catch(function(error) {
          // context.commit('createResponse', {msg: error.response.data.errors[0], was_good: false});
          console.log(error);
        });
    },
    update(context, Post) {
      Api.Post.update(Post)
        .then(response => response.data)
        .then(Post => {
          context.commit('setOnePost', Post); // not often used
          // context.commit('updateResponse', {msg: '', was_good: true});
        }).catch(function(error) {
          // context.commit('updateResponse', {msg: error.response.data.errors[0], was_good: false});
          console.log(error);
        });
    },
  }

};

export default Post;
