// import firebase from '@/firebase/index'

const Product = {
  namespaced: true,
  state: {
    allCategories: null,

    allSubcategories: null,

    allProducts: null, // [],

    newProducts: null, // [],

    oneProduct: null, // {}

    success: null,
    success_msg: null,
    error: null,
    error_msg: null,
  },

  getters: {
    getAllCategories: state => {
      return state.allCategories
    },

    getAllSubcategories: state => {
      return state.allSubcategories
    },

    getAllProducts: state => {
      return state.allProducts
    }
  },

  mutations: {
    setAllProducts (state, products) {
      state.allProducts = products
    },

    setAllSubcategories (state, subcategories) {
      state.allSubcategories = subcategories
    },

    setAllCategories (state, categories) {
      state.allCategories = categories
    },
  },

  actions: {
    setCategory (context, { item, firebase }) {
      firebase.firestore().collection("categories").add({
        ...item
      })
    },

    deleteCategory (context, { item, firebase }) {
      firebase.firestore().collection("categories").doc(item.id).delete().then(() => {
        console.log("Document successfully deleted!");
      }).catch(error => {
          console.error("Error removing document: ", error);
      });
    },

    renovateCategory (context, { item, firebase }) {
      const ref = firebase.firestore().collection("categories").doc(item.id)

      firebase.firestore().runTransaction(transaction => {
        return transaction.get(ref).then(() => {
          transaction.update(ref, { name: item.name })
          return item.name
        })
      })
    },

    setSubcategory ({ state }, { item, category, firebase }) {
      firebase.firestore().collection("subcategories").add({
        ...item,
        category_id: firebase.firestore().doc(`categories/${category.id}`),
        category_name: category.name
      }).then(() => {
        const ref = firebase.firestore().collection("categories").doc(category.id)
        const idx = state.allSubcategories.map(o=>o.name.toUpperCase()).indexOf(item.name.toUpperCase())

        category.subcategories_ids.push(firebase.firestore().doc(`subcategories/${state.allSubcategories[idx].id}`))
        category.sub_names.push(item.name)

        firebase.firestore().runTransaction(transaction => {
          return transaction.get(ref).then(() => {
            transaction.update(ref, category)
            return category
          })
        })
      })
    },

    deleteSubcategory (context, { item, firebase }) {
      const docRef = firebase.firestore().collection("subcategories").doc(item.id)
      let category = firebase.firestore().doc(item.category_id.path)

      category.get().then(doc => {
        let sub_ids = doc.data().subcategories_ids
        let sub_names = doc.data().sub_names
        const idx = sub_ids.map(o=>o.path).indexOf(`subcategories/${item.id}`)

        sub_ids.splice(idx, 1)
        sub_names.splice(idx, 1)

        firebase.firestore().runTransaction(transaction => {
          return transaction.get(category).then(() => {
            transaction.update(category, { subcategories_ids: sub_ids, sub_names: sub_names })
            return category.subcategories_ids
          })
        })
      })

      docRef.delete().then(() => {
        console.log("Document successfully deleted!");
      }).catch(error => {
          console.error("Error removing document: ", error);
      })
    },

    updateSubcategory (context, { item, category, firebase }) {
      const ref = firebase.firestore().collection("subcategories").doc(item.id)
      let {name, category_id} = item
      let obj = {name, category_id}
      if (item.changed_cat){
        obj.category_id = category.id
        const oldCategory = firebase.firestore().doc(item.category_id.path)
        const newCategory = firebase.firestore().collection("categories").doc(category.id)

        // acessar outra action de outro modulo -> {root: true}
        // context.dispatch('OutroModule/ActionDoOutroModule', {params}, {root: true})

        console.log(oldCategory.path)
        console.log(newCategory.path)

        if (oldCategory.path !== newCategory.path) {
          oldCategory.get().then(doc => {
            let sub_ids = doc.data().subcategories_ids
            let sub_names = doc.data().sub_names
            const idx = sub_ids.map(o=>o.path).indexOf(`subcategories/${item.id}`)

            sub_ids.splice(idx, 1)
            sub_names.splice(idx, 1)

            firebase.firestore().runTransaction(transaction => {
              return transaction.get(oldCategory).then(() => {
                transaction.update(oldCategory, { subcategories_ids: sub_ids, sub_names: sub_names })
                return oldCategory
              })
            })
          })

          newCategory.get().then(doc => {
            let sub_ids = doc.data().subcategories_ids
            let sub_names = doc.data().sub_names

            sub_ids.push(firebase.firestore().doc(`subcategories/${item.id}`))
            sub_names.push(item.name)

            firebase.firestore().runTransaction(transaction => {
              return transaction.get(newCategory).then(() => {
                transaction.update(newCategory, { subcategories_ids: sub_ids, sub_names: sub_names })
                return newCategory.subcategories_ids
              })
            })
          })
        }
      }

      firebase.firestore().runTransaction(transaction => {
        return transaction.get(ref).then(() => {
          transaction.update(ref, { name: item.name, category_id: firebase.firestore().doc(`categories/${obj.category_id}`), category_name: category.name })
          return item
        })
      })
    },

    setProduct (context, { item, category, subcategory, firebase }) {
      // const storageRef = firebase.storage().ref('productsImages/' + `${item.productImage.name}`)


      // console.log(storageRef)
      // const imageRef = storageRef.child(`${item.name}` + '.jpg')
      // const allImagesRef = storageRef.child('images/' + `${item.name}` + '.jpg')

      // if (item.productImage) {
      //   storageRef.put(item.productImage).then(() => {
      //     console.log('deu certo')
      //   })
      // }


      subcategory = context.state.allSubcategories.find(c => c.name == subcategory)
      firebase.firestore().collection("products").add({
        ...item,
        category_id: firebase.firestore().doc(`categories/${category.id}`),
        category_name: category.name,
        subcategory_id: firebase.firestore().doc(`subcategories/${subcategory.id}`),
        subcategory_name: subcategory.name
      })
    },

    deleteProduct (context, { item, firebase }) {
      firebase.firestore().collection("products").doc(item.id).delete().then(() => {
        console.log("Document successfully deleted!");
      }).catch(error => {
          console.error("Error removing document: ", error);
      });
    },

    updateProduct (context, { item, category, subcategory, firebase }) {
      if (!subcategory.id) {
        subcategory = context.state.allSubcategories.find(c => c.name.toUpperCase() == subcategory.toUpperCase())
      }
      item.category_name = category.name
      item.category_id = firebase.firestore().doc(`categories/${category.id}`)
      item.subcategory_name = subcategory.name
      item.subcategory_id = firebase.firestore().doc(`categories/${subcategory.id}`)
      const ref = firebase.firestore().collection("products").doc(item.id)

      firebase.firestore().runTransaction(transaction => {
        return transaction.get(ref).then(() => {
          transaction.update(ref, item)
          return item
        })
      })
    },

    index(context, firebase){
      let products_firestore = []
      let subcategories_firestore = []
      let categories_firestore = []

      firebase.firestore().collection("categories").onSnapshot(res => {
        const changes = res.docChanges()

        changes.forEach(change => {
          if (change.type === 'added') {
            categories_firestore.push({...change.doc.data(), id: change.doc.id})
          } else if (change.type === 'removed') {
            let idx = categories_firestore.map(item => item.id).indexOf(change.doc.id)

            if (idx != -1) {
              categories_firestore.splice(idx, 1)
            }
          } else if (change.type === 'modified') {
            Object.assign(categories_firestore[categories_firestore.map(o=>o.id).indexOf(change.doc.id)], {...change.doc.data(), id: change.doc.id})
          }
        })
      });

      firebase.firestore().collection("subcategories").onSnapshot(res => {
        const changes = res.docChanges()

        changes.forEach(change => {
          if (change.type === 'added') {
            subcategories_firestore.push({...change.doc.data(), id: change.doc.id})
          } else if (change.type === 'removed') {
            let idx = subcategories_firestore.map(item => item.id).indexOf(change.doc.id)

            if (idx != -1) {
              subcategories_firestore.splice(idx, 1)
            }
          } else if (change.type === 'modified') {
            Object.assign(subcategories_firestore[subcategories_firestore.map(o=>o.id).indexOf(change.doc.id)], {...change.doc.data(), id: change.doc.id})
          }
        })
      });

      firebase.firestore().collection("products").onSnapshot(res => {
        const changes = res.docChanges()

        changes.forEach(change => {
          if (change.type === 'added') {
            products_firestore.push({...change.doc.data(), id: change.doc.id})
          } else if (change.type === 'removed') {
            let idx = products_firestore.map(item => item.id).indexOf(change.doc.id)

            if (idx != -1) {
              products_firestore.splice(idx, 1)
            }
          } else if (change.type === 'modified') {
            Object.assign(products_firestore[products_firestore.map(o=>o.id).indexOf(change.doc.id)], {...change.doc.data(), id: change.doc.id})
          }
        })
      });

      context.commit('setAllProducts', products_firestore)
      context.commit('setAllSubcategories', subcategories_firestore)
      context.commit('setAllCategories', categories_firestore)

    },

    show(){},
    create(){},
    update(){},
    destroy(){},
  }

};

export default Product;
