import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/storage'
import 'firebase/database'
import 'firebase/firestore'

export const firebaseApp = firebase.initializeApp({
  apiKey: "AIzaSyAAS3xYKGcc7j9vnzDF-2LFPPMfrb3_Jgg",
  authDomain: "mercado-8fabf.firebaseapp.com",
  databaseURL: "https://mercado-8fabf.firebaseio.com",
  projectId: "mercado-8fabf",
  storageBucket: "mercado-8fabf.appspot.com",
  messagingSenderId: "559019504604",
  appId: "1:559019504604:web:2d64ec44029c791652d519",
  measurementId: "G-F8R6TBH91E"
});

export default function install (Vue) {
  Object.defineProperty(Vue.prototype, '$firebase', {
    get () {
      return firebaseApp
    }
  })
}
