import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import pt from 'vuetify/es5/locale/pt';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: '#1a719c',
        secondary: '#ECEFF1',

        accent: '#ff5722',
        error: '#f44336',
        warning: '#ffc107',
        info: '#009688',
        success: '#4caf50',

      },
    },
  },
  lang: {
    locales: { pt },
    current: 'pt',
  },
  icons: {
    iconfont: 'mdi',
  },
});
