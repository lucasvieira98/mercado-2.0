import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'ProductList',
    component: () => import(/* webpackChunkName: "product-list" */ '../views/UserProductList.vue')
  },
  {
    path: '/add',
    name: 'AddProduct',
    component: () => import(/* webpackChunkName: "about" */ '../views/AddProduct.vue'),
    children: [
      {
        path: ':kind',
        props: true,
        name: 'AddGeneral',
        component: () => import(/* webpackChunkName: "add_product" */ '../components/product/add.vue'),
      },
    ]
  },
  {
    path: '/my_carts',
    name: 'MyCarts',
    component: () => import(/* webpackChunkName: "my_carts" */ '../views/myCarts.vue'),
    children: [
      {
        path: ':kind',
        props: true,
        name: 'MyCartsGeneral',
        component: () => import(/* webpackChunkName: "my_carts_general" */ '../components/cart/my_carts/MyCartsGeneral.vue'),
      },
    ]
  },

  {
    path: '/verify',
    name: 'VerifyCarts',
    component: () => import(/* webpackChunkName: "verify_carts" */ '../views/VerifyCarts.vue'),
    children: [
      {
        path: ':kind',
        props: true,
        name: 'VerifyGeneralGeneral',
        component: () => import(/* webpackChunkName: "verify_general" */ '../components/cart/verify/VerifyGeneral.vue'),
      },
    ]
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "login" */ '../views/Login.vue')
  },
  // tem que fazer a rota settings com os children para cadastrar produtos, cat, subcat, users(offline)

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
